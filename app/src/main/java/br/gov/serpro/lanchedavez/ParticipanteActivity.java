package br.gov.serpro.lanchedavez;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ParticipanteActivity extends AppCompatActivity {

    private ArrayList<Participante> participantesArray = new ArrayList<Participante>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participante);

        if(this.participantesArray == null || this.participantesArray.isEmpty()){
            preecherListaParticipantes(this);
        }

        ParticipanteAdapter adapter = new ParticipanteAdapter(this, this.participantesArray);
        ListView listViewParticipante = (ListView) findViewById(R.id.list_viewParticipante);
        listViewParticipante.setAdapter(adapter);

        listViewParticipante.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(ParticipanteActivity.this,AgendarEventoActivity.class);

                Participante p = participantesArray.get(position);

                intent.putExtra("participante", p);

                startActivity(intent);
            }
        });
    }

    public void preecherListaParticipantes(Context contexto){
        ParticipantesDBHelper participantesDBHelper = new ParticipantesDBHelper(this);
        Cursor cursor = participantesDBHelper.buscarParticipantes(this);

        if(cursor != null && cursor.moveToNext()){
            cursor.moveToPrevious();
            while(cursor.moveToNext()) {
                Participante participante = new Participante();
                participante.setId(cursor.getLong(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry._ID)));
                participante.setNome(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_NOME)));
                participante.setEmail(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL)));
                participante.setTelefone(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE)));
                participante.setFoto(cursor.getInt(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_FOTO)));
                participantesArray.add(participante);
            }
            cursor.close();
        }else {
            inicializarTabelaParticipante(this);
            preecherListaParticipantes(contexto);
        }
    }

    public void inicializarTabelaParticipante(Context contexto){
        ParticipantesDBHelper participantesDBHelper = new ParticipantesDBHelper(this);

        ArrayList<Participante> participantes = new ArrayList<Participante>();
        participantes.add(new Participante("Daniela Machado","11 2173-1814","daniela.machado@serpro.gov.br",R.drawable.daniela_machado));
        participantes.add(new Participante("Flávio Augusto","11 2173-1840","flavio-augusto.silva@serpro.gov.br",R.drawable.flavio_augusto_silva));
        participantes.add(new Participante("Gina Moraes","11 2173-1292","gina.moraes@serpro.gov.br",R.drawable.gina_moraes));
        participantes.add(new Participante("Glauco Oliveira","11 2173-3763","glauco.oliveira@serpro.gov.br",R.drawable.glauco_silva_de_oliveira));

        for(Participante participante : participantes){
            long idParticipante = participantesDBHelper.inserirParticipante(participante, contexto);
            Log.d("LOG", "idParticipante=" + String.valueOf(idParticipante));
        }
    }
}
