package br.gov.serpro.lanchedavez;

import android.provider.BaseColumns;

/**
 * Created by aluno on 09/06/16.
 */
public final class ParticipanteContract {
    public ParticipanteContract() {}

    // Define a tabela de participantes
    public static abstract class ParticipantesEntry implements BaseColumns {
        public static final String TABLE_NAME = "participantes";
        public static final String COLUMN_NOME = "nome";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_TELEFONE = "telefone";
        public static final String COLUMN_FOTO = "foto";
    }

}
