package br.gov.serpro.lanchedavez;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by aluno on 09/06/16.
 */
public class ParticipantesDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1; // Ao mudar o esquema do banco, incrementar o número da versão
    public static final String DATABASE_NAME = "participante.db";

    private static final String SQL_CRIAR_BANCO =
            "CREATE TABLE " + ParticipanteContract.ParticipantesEntry.TABLE_NAME +
                    " (" + ParticipanteContract.ParticipantesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ParticipanteContract.ParticipantesEntry.COLUMN_NOME + " TEXT NOT NULL" + ", "
                    + ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL + " TEXT NOT NULL" + ", "
                    + ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE + " BOOLEAN NOT NULL DEFAULT 0" + ", "
                    + ParticipanteContract.ParticipantesEntry.COLUMN_FOTO + " INTEGER" + ")";
    private static final String SQL_APAGAR_TABELA =
            "DROP TABLE " + EventoContract.EventosEntry.TABLE_NAME;

    public ParticipantesDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CRIAR_BANCO);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL(SQL_APAGAR_TABELA);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) { // não necessário
        onUpgrade(db, oldVersion, newVersion);
    }

    public long inserirParticipante(Participante participante, Context contexto) {
        ParticipantesDBHelper dbHelper = new ParticipantesDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // modo de gravação

        // Cria um novo mapa de valores para serem inseridos no banco
        ContentValues values = new ContentValues();
        values.put(ParticipanteContract.ParticipantesEntry.COLUMN_NOME, participante.getNome());
        values.put(ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL, participante.getEmail());
        values.put(ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE, participante.getTelefone());
        values.put(ParticipanteContract.ParticipantesEntry.COLUMN_FOTO, participante.getFoto());

        // Insere a nova linha, retornando a chave primária dessa nova linha
        return db.insert(ParticipanteContract.ParticipantesEntry.TABLE_NAME, "null", values);
    }

    public Cursor buscarParticipantes(Context contexto){
        ParticipantesDBHelper dbHelper = new ParticipantesDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Define quais colunas serão retornadas
        String[] projection = {
                ParticipanteContract.ParticipantesEntry._ID,
                ParticipanteContract.ParticipantesEntry.COLUMN_NOME,
                ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL,
                ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE,
                ParticipanteContract.ParticipantesEntry.COLUMN_FOTO,
        };

        String sortOrder = ParticipanteContract.ParticipantesEntry.COLUMN_NOME + " ASC";

        return db.query(
                ParticipanteContract.ParticipantesEntry.TABLE_NAME,  // a tabela da query
                projection,                              // as colunas que serão retornadas
                null,                                   // as colunas para a cláusula WHERE
                null,                                     // os valores da cláusula WHERE
                null,                                    // não agrupar as linhas
                null,                                    // não filtrar por grupos de linhas
                sortOrder                                // ordenação
        );
    }

    public Cursor consultarParticipante(Participante participante, Context contexto) throws SQLException {
        ParticipantesDBHelper dbHelper = new ParticipantesDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Define quais colunas serão retornadas
        String[] projection = {
                ParticipanteContract.ParticipantesEntry._ID,
                ParticipanteContract.ParticipantesEntry.COLUMN_NOME,
                ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL,
                ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE,
                ParticipanteContract.ParticipantesEntry.COLUMN_FOTO,
        };

        String where = ParticipanteContract.ParticipantesEntry._ID + "=?";
        String[] whereArgs = {String.valueOf(participante.getId())};

        return db.query(
                ParticipanteContract.ParticipantesEntry.TABLE_NAME,  // a tabela da query
                projection,                              // as colunas que serão retornadas
                where,                                    // as colunas para a cláusula WHERE
                whereArgs,                                    // os valores da cláusula WHERE
                null,                                    // não agrupar as linhas
                null,                                    // não filtrar por grupos de linhas
                null                                     // ordenação
        );
    }
}

