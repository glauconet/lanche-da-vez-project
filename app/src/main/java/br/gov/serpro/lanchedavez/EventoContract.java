package br.gov.serpro.lanchedavez;

import android.provider.BaseColumns;

/**
 * Created by aluno on 09/06/16.
 */
public final class EventoContract {
    public EventoContract() {}

    // Define a tabela de eventos
    public static abstract class EventosEntry implements BaseColumns {
        public static final String TABLE_NAME = "eventos";
        public static final String COLUMN_PARTICIPANTE = "id_participante";
        public static final String COLUMN_DATA = "data";
        public static final String COLUMN_IN_DOCE = "in_doce";
        public static final String COLUMN_IN_SALGADO = "in_salgado";
        public static final String COLUMN_DESC_DOCE = "desc_doce";
        public static final String COLUMN_DESC_SALGADO = "desc_salgado";
    }

}
