package br.gov.serpro.lanchedavez;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aluno on 09/06/16.
 */
public class ParticipanteAdapter extends ArrayAdapter<Participante> {
    public ParticipanteAdapter(Context context, ArrayList<Participante> Participantes) {
        super(context, 0, Participantes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Participante participante = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_participante_layout, parent, false);
        }

        TextView tvId = (TextView) convertView.findViewById(R.id.participanteId);
        TextView tvNome = (TextView) convertView.findViewById(R.id.participanteNome);
        TextView tvTelefone = (TextView) convertView.findViewById(R.id.participanteTelefone);
        TextView tvEmail = (TextView) convertView.findViewById(R.id.participanteEmail);
        ImageView tvFoto = (ImageView) convertView.findViewById(R.id.participanteFoto);

        tvId.setText(String.valueOf(participante.getId()));
        tvNome.setText(participante.getNome());
        tvTelefone.setText(participante.getTelefone());
        tvEmail.setText(participante.getEmail());
        tvFoto.setImageResource(participante.getFoto());

        return convertView;
    }
}

