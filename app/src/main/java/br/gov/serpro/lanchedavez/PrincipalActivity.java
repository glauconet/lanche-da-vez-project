package br.gov.serpro.lanchedavez;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class PrincipalActivity extends AppCompatActivity {

    private final String  TAG = "LOG";
    public List <Evento> eventos = new ArrayList<Evento>();
    private EventoAdapter adpater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Agendar Evento", Snackbar.LENGTH_LONG).setAction("Agendar Evento", null).show();
                Intent intent = new Intent(PrincipalActivity.this, ParticipanteActivity.class);
                startActivity(intent);
            }
        });

        buscarEventos();

        final ListView listView= (ListView) findViewById(R.id.lista_evento);

        adpater = new EventoAdapter(this, eventos);

        listView.setAdapter(adpater);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(PrincipalActivity.this,AgendarEventoActivity.class);
                Evento e = eventos.get(position);
                intent.putExtra("evento", e);
                startActivity(intent);
            }
        });
    }

    public void buscarEventos(){
        EventosDBHelper eventosDBHelper = new EventosDBHelper(this);
        Cursor cursor = eventosDBHelper.buscarEventos(this);

        while(cursor.moveToNext()){
            Evento evento = new Evento();

            evento.setParticipante(new Participante(cursor.getLong(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_PARTICIPANTE))));
            evento.setParticipante(this.recuperarParticipante(evento.getParticipante(), this));

            String data = cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_DATA));
            int length = data.length();
            evento.setData(data.substring(0,length));
            evento.setDescDoce(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_DESC_DOCE)));
            evento.setDescSalgado(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_DESC_SALGADO)));
            if(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_IN_DOCE)) == "1"){
                evento.setInDoce(true);
            }
            if(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_IN_SALGADO)) == "1"){
                evento.setInSalgado(true);
            }
            evento.setId(cursor.getLong(cursor.getColumnIndex(EventoContract.EventosEntry._ID)));
            this.eventos.add(evento);
        }
        cursor.close();
        eventosDBHelper.close();
    }

    public Participante recuperarParticipante(Participante participante, Context contexto){
        ParticipantesDBHelper participantesDBHelper = new ParticipantesDBHelper(this);
        Log.d(TAG, "LOG parcelable  != null, então participante id igual a " + participante.getId());
        Cursor cursor = participantesDBHelper.consultarParticipante(participante, contexto);

        Participante part = new Participante();
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) { // If you use c.moveToNext() here, you will bypass the first row, which is WRONG
                part.setId(cursor.getLong(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry._ID)));
                part.setNome(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_NOME)));
                Log.d(TAG, "LOG parcelable  != null, então participante nome igual a " + part.getNome());
                part.setEmail(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL)));
                part.setTelefone(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE)));
                part.setFoto(cursor.getInt(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_FOTO)));
                cursor.moveToNext();
            }
        }
        cursor.close();
        participantesDBHelper.close();
        return part;
    }

    @Override
    protected void onRestart() {
        Log.d(TAG,"LOG: onRestart ") ;
        super.onRestart();
        adpater.notifyDataSetChanged();
    }
}
