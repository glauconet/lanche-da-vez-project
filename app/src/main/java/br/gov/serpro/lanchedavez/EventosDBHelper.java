package br.gov.serpro.lanchedavez;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by aluno on 09/06/16.
 */
public class EventosDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1; // Ao mudar o esquema do banco, incrementar o número da versão
    public static final String DATABASE_NAME = "eventos.db";

    private static final String SQL_CRIAR_BANCO =
            "CREATE TABLE " + EventoContract.EventosEntry.TABLE_NAME +
                    " (" + EventoContract.EventosEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + EventoContract.EventosEntry.COLUMN_PARTICIPANTE + " TEXT NOT NULL" + ", "
                    + EventoContract.EventosEntry.COLUMN_DATA + " TEXT NOT NULL" + ", "
                    + EventoContract.EventosEntry.COLUMN_IN_DOCE + " BOOLEAN NOT NULL DEFAULT 0" + ", "
                    + EventoContract.EventosEntry.COLUMN_IN_SALGADO + " BOOLEAN NOT NULL DEFAULT 0" + ", "
                    + EventoContract.EventosEntry.COLUMN_DESC_DOCE + " TEXT " + ", "
                    + EventoContract.EventosEntry.COLUMN_DESC_SALGADO + " TEXT " +
                    " )";

    private static final String SQL_APAGAR_TABELA =
            "DROP TABLE " + EventoContract.EventosEntry.TABLE_NAME;

    public EventosDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CRIAR_BANCO);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL(SQL_APAGAR_TABELA);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) { // não necessário
        onUpgrade(db, oldVersion, newVersion);
    }

    public long inserirEvento(Evento evento, Context contexto) {
        EventosDBHelper dbHelper = new EventosDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // modo de gravação

        // Cria um novo mapa de valores para serem inseridos no banco
        ContentValues values = new ContentValues();
        values.put(EventoContract.EventosEntry.COLUMN_PARTICIPANTE, evento.getParticipante().getId());
        values.put(EventoContract.EventosEntry.COLUMN_DATA, evento.getData());
        values.put(EventoContract.EventosEntry.COLUMN_IN_DOCE, evento.isInDoce());
        values.put(EventoContract.EventosEntry.COLUMN_IN_SALGADO, evento.isInSalgado());
        values.put(EventoContract.EventosEntry.COLUMN_DESC_DOCE, evento.getDescDoce());
        values.put(EventoContract.EventosEntry.COLUMN_DESC_SALGADO, evento.getDescSalgado());

        // Insere a nova linha, retornando a chave primária dessa nova linha
        return db.insert(EventoContract.EventosEntry.TABLE_NAME, "null", values);
    }

    public long atualizarEvento(Evento evento, Context contexto) {
        EventosDBHelper dbHelper = new EventosDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // modo de gravação

        // Cria um novo mapa de valores para serem inseridos no banco
        ContentValues values = new ContentValues();
        values.put(EventoContract.EventosEntry.COLUMN_PARTICIPANTE, evento.getParticipante().getId());
        values.put(EventoContract.EventosEntry.COLUMN_DATA, evento.getData());
        values.put(EventoContract.EventosEntry.COLUMN_IN_DOCE, evento.isInDoce());
        values.put(EventoContract.EventosEntry.COLUMN_IN_SALGADO, evento.isInSalgado());
        values.put(EventoContract.EventosEntry.COLUMN_DESC_DOCE, evento.getDescDoce());
        values.put(EventoContract.EventosEntry.COLUMN_DESC_SALGADO, evento.getDescSalgado());

        String where = EventoContract.EventosEntry._ID + "=?";
        String[] whereArgs = new String[]{String.valueOf(evento.getId())};

        // Insere a nova linha, retornando a chave primária dessa nova linha
        return db.update(
                EventoContract.EventosEntry.TABLE_NAME,
                values,
                where, whereArgs);
    }

    public Cursor buscarEventos(Context contexto){
        EventosDBHelper dbHelper = new EventosDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Define quais colunas serão retornadas
        String[] projection = {
                EventoContract.EventosEntry._ID,
                EventoContract.EventosEntry.COLUMN_PARTICIPANTE,
                EventoContract.EventosEntry.COLUMN_DATA,
                EventoContract.EventosEntry.COLUMN_IN_DOCE,
                EventoContract.EventosEntry.COLUMN_DESC_DOCE,
                EventoContract.EventosEntry.COLUMN_IN_SALGADO,
                EventoContract.EventosEntry.COLUMN_DESC_SALGADO
        };

        String sortOrder = EventoContract.EventosEntry.COLUMN_DATA + " DESC";

        return db.query(
                EventoContract.EventosEntry.TABLE_NAME,  // a tabela da query
                projection,                              // as colunas que serão retornadas
                null,                                   // as colunas para a cláusula WHERE
                null,                                     // os valores da cláusula WHERE
                null,                                    // não agrupar as linhas
                null,                                    // não filtrar por grupos de linhas
                sortOrder                                // ordenação
        );
    }

    public Cursor consultarEvento(Evento evento, Context contexto) throws SQLException {
        EventosDBHelper dbHelper = new EventosDBHelper(contexto);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Define quais colunas serão retornadas
        String[] projection = {
                EventoContract.EventosEntry._ID,
                EventoContract.EventosEntry.COLUMN_PARTICIPANTE,
                EventoContract.EventosEntry.COLUMN_DATA,
                EventoContract.EventosEntry.COLUMN_IN_DOCE,
                EventoContract.EventosEntry.COLUMN_DESC_DOCE,
                EventoContract.EventosEntry.COLUMN_IN_SALGADO,
                EventoContract.EventosEntry.COLUMN_DESC_SALGADO
        };

        String where = EventoContract.EventosEntry._ID + "=?";
        String[] whereArgs = {String.valueOf(evento.getId())};

        return db.query(
                EventoContract.EventosEntry.TABLE_NAME,  // a tabela da query
                projection,                              // as colunas que serão retornadas
                where,                                   // as colunas para a cláusula WHERE
                whereArgs,                               // os valores da cláusula WHERE
                null,                                    // não agrupar as linhas
                null,                                    // não filtrar por grupos de linhas
                null                                     // ordenação
        );
    }
}

