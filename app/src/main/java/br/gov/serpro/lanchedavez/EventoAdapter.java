package br.gov.serpro.lanchedavez;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by aluno on 09/06/16.
 */
public class EventoAdapter extends ArrayAdapter<Evento> {

    public EventoAdapter(Context context, List<Evento> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Evento evento = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_evento_layout, parent, false);
        }

        TextView dia_semana = (TextView) convertView.findViewById(R.id.dia_semana);
        TextView dia_mes = (TextView) convertView.findViewById(R.id.dia_mes);
        TextView nome_participante =  (TextView) convertView.findViewById(R.id.nome_participante);

        dia_semana.setText(evento.getDiadaSemana());
        dia_mes.setText(String.valueOf(evento.getDiaMes()));
        nome_participante.setText(evento.getParticipante().getNome());

        return convertView;
    }
}
