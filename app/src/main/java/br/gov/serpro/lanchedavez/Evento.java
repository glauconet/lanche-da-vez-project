package br.gov.serpro.lanchedavez;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;

import java.text.DateFormatSymbols;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static java.util.Calendar.*;

/**
 * Created by aluno on 09/06/16.
 */
public class Evento implements Parcelable {

    private long id;
    private String data;
    private Participante participante;
    private boolean inDoce;
    private boolean inSalgado;
    private String descDoce;
    private String descSalgado;

    public Evento() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.data);
        dest.writeLong(this.participante.getId());
        dest.writeString(String.valueOf(this.inDoce));
        dest.writeString(String.valueOf(this.inSalgado));
        dest.writeString(String.valueOf(this.descDoce));
        dest.writeString(String.valueOf(this.descSalgado));
    }

    protected Evento(Parcel in) {
        this.id = in.readLong();
        this.data = in.readString();
        this.participante = new Participante(in.readLong());
        this.inDoce = in.readByte() != 0;
        this.inSalgado = in.readByte() != 0;
        this.descDoce = in.readString();
        this.descSalgado = in.readString();
    }

    public static final Creator<Evento> CREATOR = new Creator<Evento>() {
        @Override
        public Evento createFromParcel(Parcel source) {
            return new Evento(source);
        }

        @Override
        public Evento[] newArray(int size) {
            return new Evento[size];
        }
    };

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescDoce() {
        return descDoce;
    }

    public void setDescDoce(String descDoce) {
        this.descDoce = descDoce;
    }

    public String getDescSalgado() {
        return descSalgado;
    }

    public void setDescSalgado(String descSalgado) {
        this.descSalgado = descSalgado;
    }

    public String getDiaMes() {
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyyy");
        Date dataEvento = simpledateformat.parse(this.getData(), pos);

        final Calendar c = getInstance();
        c.setTime(dataEvento);
        return c.get(DAY_OF_MONTH) + "/" +  (c.get(MONTH) + 1);
    }

    public String getDiadaSemana() {
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyyy");
        Date dataEvento = simpledateformat.parse(this.getData(), pos);

        final Calendar c = getInstance();
        c.setTime(dataEvento);
        String[] dias = new DateFormatSymbols(Locale.getDefault()).getWeekdays();
        return dias[c.get(DAY_OF_WEEK)].substring(0, 3);

        //return SimpleDateFormat(Locale.getDefault(), "EE").format(c.getTime());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isInDoce() {
        return inDoce;
    }

    public void setInDoce(boolean inDoce) {
        this.inDoce = inDoce;
    }

    public boolean isInSalgado() {
        return inSalgado;
    }

    public void setInSalgado(boolean inSalgado) {
        this.inSalgado = inSalgado;
    }

    public Participante getParticipante() {
        return participante;
    }

    public void setParticipante(Participante participante) {
        this.participante = participante;
    }
}
