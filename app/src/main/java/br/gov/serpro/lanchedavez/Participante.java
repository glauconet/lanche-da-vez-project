package br.gov.serpro.lanchedavez;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aluno on 08/06/16.
 */
public class Participante implements Parcelable {

    public long id;
    private String nome;
    private String email;
    private String telefone;
    private int foto;

    public Participante() {
    }

    public Participante(long id){
        this.id = id;
    }

    public Participante(String nome, String telefone, String email, int foto) {
        this.email = email;
        this.foto = foto;
        this.nome = nome;
        this.telefone = telefone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.nome);
        dest.writeString(this.email);
        dest.writeString(this.telefone);
        dest.writeInt(this.foto);
    }

    protected Participante(Parcel in) {
        this.id = in.readLong();
        this.nome = in.readString();
        this.email = in.readString();
        this.telefone = in.readString();
        this.foto = in.readInt();
    }

    public static final Parcelable.Creator<Participante> CREATOR = new Parcelable.Creator<Participante>() {
        @Override
        public Participante createFromParcel(Parcel source) {
            return new Participante(source);
        }

        @Override
        public Participante[] newArray(int size) {
            return new Participante[size];
        }
    };
}
