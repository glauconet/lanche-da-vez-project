package br.gov.serpro.lanchedavez;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Telephony;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;                                                   
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AgendarEventoActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private final String TAG = "LANCHEDAVEZ";
    private Button salvar;
    private TextView salgado_text;
    private TextView doce_text;
    private TextView data;
    private TextView nome_participante;
    private CheckBox in_salgado;
    private CheckBox in_doce;
    private ImageView botaoDate;
    private ImageView fotoParticipante;

    private Evento evento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendar_evento);

        Intent i = getIntent();
        salgado_text = (TextView) findViewById(R.id.salgado_text);
        doce_text = (TextView) findViewById(R.id.doce_text);
        nome_participante = (TextView) findViewById(R.id.nome_participante);
        in_salgado = (CheckBox) findViewById(R.id.doce_label);
        in_doce = (CheckBox) findViewById(R.id.salgado_label);
        data = (TextView) findViewById(R.id.date_picker);
        fotoParticipante = (ImageView) findViewById(R.id.foto_participante) ;
        //data.setText( new Date().toString());
        botaoDate = (ImageView) findViewById(R.id.date_picker_button);

        salvar = (Button) findViewById(R.id.salvar);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrar(v);
            }
        });

        if (i.getParcelableExtra("participante") != null) {
            Participante participante = (Participante) i.getParcelableExtra("participante");
            Log.d(TAG, "LOG parcelable  != null, então participante id igual a " + participante.getId());

            this.evento = new Evento();
            this.evento.setParticipante(participante);

            Log.d(TAG, "LOG parcelable  != null, então participante2 id igual a " + participante.getId());

            nome_participante.setText(this.evento.getParticipante().getNome());
            fotoParticipante.setImageResource(this.evento.getParticipante().getFoto());

        }else if (i.getParcelableExtra("evento") != null) {
            this.evento = (Evento) i.getParcelableExtra("evento");

            Log.d(TAG, "LOG parcelable  != null, então evento id igual a " + this.evento.getId());
            Log.d(TAG, "LOG parcelable  != null, então evento idParticipante igual a " + this.evento.getParticipante().getId());

            this.evento = recuperarEvento(this.evento, this);
            this.evento.setParticipante(recuperarParticipante(this.evento.getParticipante(), this));

            Log.d(TAG, "LOG parcelable  != null, então evento id igual a " + this.evento.getId());
            Log.d(TAG, "LOG parcelable  != null, então evento idParticipante igual a " + this.evento.getParticipante().getId());
            Log.d(TAG, "LOG parcelable  != null, então evento nomeParticipante igual a " + this.evento.getParticipante().getNome());
            Log.d(TAG, "LOG parcelable  != null, então evento descDoce igual a " + this.evento.getDescDoce());
            Log.d(TAG, "LOG parcelable  != null, então evento descSalgado igual a " + this.evento.getDescSalgado());

            nome_participante.setText(evento.getParticipante().getNome());
            fotoParticipante.setImageResource(evento.getParticipante().getFoto());
            salgado_text.setText(evento.getDescSalgado());
            doce_text.setText(evento.getDescDoce());
            in_salgado.setChecked(evento.isInSalgado());
            in_doce.setChecked(evento.isInDoce());
            data.setText(evento.getData());
        }
    }

    public Participante recuperarParticipante(Participante participante, Context contexto){
        ParticipantesDBHelper participantesDBHelper = new ParticipantesDBHelper(this);
        Cursor cursor = participantesDBHelper.consultarParticipante(participante, contexto);

        Participante part = new Participante();
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) { // If you use c.moveToNext() here, you will bypass the first row, which is WRONG
                Log.d(TAG, "ENTROU NO WHILE PARTICIPANTE");
                part.setId(cursor.getLong(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry._ID)));
                part.setNome(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_NOME)));
                part.setEmail(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_EMAIL)));
                part.setTelefone(cursor.getString(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_TELEFONE)));
                part.setFoto(cursor.getInt(cursor.getColumnIndex(ParticipanteContract.ParticipantesEntry.COLUMN_FOTO)));
                cursor.moveToNext();
            }
        }
        cursor.close();
        participantesDBHelper.close();
        return part;
    }

    public Evento recuperarEvento(Evento evento, Context contexto){
        EventosDBHelper eventoDBHelper = new EventosDBHelper(this);
        Cursor cursor = eventoDBHelper.consultarEvento(evento, contexto);

        Evento ev = new Evento();
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) { // If you use c.moveToNext() here, you will bypass the first row, which is WRONG
                Log.d(TAG, "ENTROU NO WHILE");
                ev.setId(cursor.getLong(cursor.getColumnIndex(EventoContract.EventosEntry._ID)));
                ev.setData(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_DATA)));
                ev.setParticipante(new Participante(cursor.getLong(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_PARTICIPANTE))));

                int inDoce = cursor.getInt(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_IN_DOCE));
                int inSalgado = cursor.getInt(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_IN_SALGADO));
                if(inDoce == 1){
                    ev.setInDoce(true);
                }
                if(inSalgado == 1){
                    ev.setInSalgado(true);
                }
                Log.d(TAG, "LOG parcelable  != null, então evento inDoce igual a " + ev.isInDoce());
                Log.d(TAG, "LOG parcelable  != null, então evento inSalgado igual a " + ev.isInSalgado());

                ev.setDescDoce(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_DESC_DOCE)));
                ev.setDescSalgado(cursor.getString(cursor.getColumnIndex(EventoContract.EventosEntry.COLUMN_DESC_SALGADO)));
                cursor.moveToNext();
            }
        }
        cursor.close();
        eventoDBHelper.close();
        return ev;
    }

    public void cadastrar(View view) {

        this.evento.setData(data.getText().toString());
        this.evento.setDescSalgado(salgado_text.getText().toString());
        this.evento.setDescDoce(doce_text.getText().toString());
        this.evento.setInSalgado(in_salgado.isChecked());
        this.evento.setInDoce(in_doce.isChecked());

        EventosDBHelper eventosDBHelper = new EventosDBHelper(this);
        if(this.evento.getId() == 0){
            evento.setId(eventosDBHelper.inserirEvento(evento, this));
            Log.d(TAG, "LOG insert ---id igual " + evento.getId());
            Log.d(TAG, "LOG insert ---idParticipante igual " + evento.getParticipante().getId());
        }else{
            eventosDBHelper.atualizarEvento(evento, this);
            Log.d(TAG, "LOG update --- id igual " + evento.getId());
        }

        Intent intent = new Intent(this, PrincipalActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        setDate(cal);
    }
    private void setDate(final Calendar calendar) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        ((TextView) findViewById(R.id.date_picker)).setText(dateFormat.format(calendar.getTime()));

    }
    public void datePicker(View view){

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "date");
    }
    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dt = new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener) getActivity(),  year, month, day);
            // return new DatePickerDialog(getActivity(),
            //        (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);

            return dt;
        }
    }
}
